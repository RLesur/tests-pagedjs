# tests-pagedjs

This repository contains my personal tests with `paged.js`.

The goal of these tests is to use `paged.js` with the [`rmarkdown`](https://rmarkdown.rstudio.com/)  and [`bookdown`](https://bookdown.org/yihui/bookdown/) `R` packages created by [Yihui Xie](http://yihui.name/).
